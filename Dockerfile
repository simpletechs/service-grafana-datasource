FROM mhart/alpine-node:8
LABEL maintainer="Fabian Off <fabian@simpletechs.net>"

# to keep this image clean, the build should be done beforehand, avoids all dev dependencies
WORKDIR /srv/webservice

COPY package.json yarn.lock ./
RUN npm install --global yarn; yarn --production

COPY src/ src/

USER nobody

ENV NODE_ENV "production"

EXPOSE 8000

CMD ["/usr/bin/node", "."]