const Koa = require('koa')
const koaBody = require('koa-body')
const router = require('koa-router')()
const morgan = require('koa-morgan')

const requestMetadataValues = require('./lib/metadata-client')

const app = new Koa();

// this enables per-request logging
morgan.token('jwt', function(req, res) {
    return (req.headers['authorization'] || '').length || 0
});
app.use(morgan(':req[x-smpl-request-id] (:remote-addr) :method :url :status :response-time ms :req[x-smpl-user-id] #:jwt - :res[content-length]'))

router.get('/', (ctx) => {
    ctx.body = 'This is METADATA'
})
router.post('/search', koaBody(),
  async (ctx) => {
    const body = ctx.request.body

    if(body.target) {
        const target = JSON.parse(body.target)

        if(target.metadata && target.values && target.key) {
            const values = target.values.replace(/\(|\)/g, '').split('|')
            ctx.body = await requestMetadataValues(target.metadata, values, target.key)
            return
        }
    }
    ctx.body = 'Err, wrong parameters'
  }
)
app.use(router.routes())

app.listen(process.env.PORT || 8000)
