const request = require('request-promise')

const agentOptions = {}
const METADATA_URL = process.env.METADATA_URL || 'http://rancher-metadata/latest'

if(process.env.CLIENT_SSL) {
    const fs = require('fs');
    Object.assign(agentOptions, {
        key: fs.readFileSync(`${process.env.CLIENT_SSL}.key.pem`),
        cert: fs.readFileSync(`${process.env.CLIENT_SSL}.crt.pem`),
        securityOptions: 'SSL_OP_NO_SSLv3'
    })
}

async function requestMetadataValues(metadata = '', values = [], key = '') {
    const resolvedValues = await Promise.all(values.map((value) => requestMetadataValue(metadata, value, key)))

    return resolvedValues.map((value, index) => ({
        text: values[index],
        value
    }))
}

function requestMetadataValue(metadata = '', value = '', key = '') {
    return request(`${METADATA_URL}/${metadata}/${value}/${key}`, { agentOptions }).catch((err) => (err && err.message) || 'ERR')
}

module.exports = requestMetadataValues
